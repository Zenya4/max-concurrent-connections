package com.Zenya4.MaxConcurrentConnections;

import java.util.ArrayList;
import java.util.List;
//import java.util.concurrent.TimeUnit;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ConcurrentConnectionsManager implements Listener {
	
	@EventHandler
	public void onServerSwitchEvent(ServerSwitchEvent e) {
		List<String> ips = new ArrayList<String>(); //List of IPs of all players connected to proxy
		List<ProxiedPlayer> concurrent = new ArrayList<ProxiedPlayer>(); //List of players with the same IP
		String ip = e.getPlayer().getAddress().getAddress().getHostAddress(); //Get connection IP
		
		for(ProxiedPlayer p : ProxyServer.getInstance().getPlayers()) {
			String tip = p.getAddress().getAddress().getHostAddress();
			ips.add(tip);
			if(ip.equals(tip)) {
				concurrent.add(p);
			}
		}

		int con = concurrent.size();
		if(con <= 1) {
			return;
		}
		else {
			for(ProxiedPlayer p : concurrent) {
				if(p.hasPermission("connections.limit.bypass")) {
					return;
				}

				for(int i = con; i <= 9; i++ ) {
					if(p.hasPermission("connections.limit." + String.valueOf(i))) {
						return;
					}
				}
			}
			e.getPlayer().disconnect(new TextComponent(ChatColor.DARK_RED + "You are already connected to the server!\n\nIf you think this is a mistake, let us know at http://discord.ham5teak.xyz"));
		}
	}
}
